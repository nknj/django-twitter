from django.conf.urls import patterns, url

urlpatterns = patterns(
    'tweets.views',
    url(r'^create/', 'create', name='create'),
)