from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from tweets.forms import TweetCreationForm
from tweets.models import Tweet


@login_required
def feed(request):
    tweet_form = TweetCreationForm()
    return render(request, 'feed.html', {'tweet_form': tweet_form})


@login_required()
def create(request):
    if request.method == 'POST':
        tweet = Tweet(user=request.user)
        tweet_form = TweetCreationForm(request.POST, instance=tweet)
        if tweet_form.is_valid():
            tweet_form.save()
            return redirect('home')
        else:
            return render(request, 'create.html', {'tweet_form': tweet_form})
    elif request.method == 'GET':
        tweet_form = TweetCreationForm()
        return render(request, 'create.html', {'tweet_form': tweet_form})