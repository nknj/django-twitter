from django.db import models
from django.utils import timezone


class Tweet(models.Model):
    user = models.ForeignKey('users.TwitterUser')
    content = models.CharField(max_length='140')
    created_at = models.DateTimeField(default=timezone.now())
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.content

    def get_mentioned_usernames(self):
        # TODO: change to something in which order is preserved? Set or Tuple or something?
        return [mention for mention in self.content.split() if mention.startswith('@')]


class Mention(models.Model):
    tweet = models.ForeignKey('Tweet')
    order_number = models.PositiveIntegerField()
    user = models.ForeignKey('users.TwitterUser', null=True)