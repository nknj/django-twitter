from django import forms
from django.utils.translation import ugettext_lazy as _
from tweets.models import Tweet, Mention
from users.models import TwitterUser


class TweetCreationForm(forms.ModelForm):
    """
    A form that creates a tweet, for a logged-in user.
    """

    content = forms.CharField(
        label=_("Content"),
        max_length=140,
        widget=forms.Textarea,
        help_text=_("Required. 140 characters or fewer. Use @ to mention another user"),
        error_messages={
            'invalid': _("This value may contain only letters and numbers.")})

    class Meta:
        model = Tweet
        fields = ("content",)

    def save(self, commit=True):
        tweet = super(TweetCreationForm, self).save(commit=commit)
        mentions = tweet.get_mentioned_usernames()
        for (i, mention) in enumerate(mentions):
            try:
                user = TwitterUser.objects.get(username=mention[1:])
            except TwitterUser.DoesNotExist:
                user = None
            m = Mention(tweet=tweet, order_number=i, user=user)
            m.save()
        return tweet