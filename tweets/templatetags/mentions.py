from django import template
from tweets.models import Mention

register = template.Library()


@register.filter
def linked_tweet(tweet):
    # TODO: two mentions of the same person
    # TODO: @colin, is not detected due to the comma at the end
    mentions = Mention.objects.filter(tweet=tweet).order_by('order_number')
    mention_strings = tweet.get_mentioned_usernames()
    for (i, mention) in enumerate(mentions):
        to_replace = mention_strings[i]
        if mention.user is not None:
            tweet.content = tweet.content.replace(
                to_replace,
                "<a href='{url}'>{mention_string}</a>".format(
                    url=mention.user.get_absolute_url(),
                    mention_string=to_replace
                ))
    return tweet.content