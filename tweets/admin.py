from django.contrib import admin
from tweets.models import Tweet


class TweetAdmin(admin.ModelAdmin):
    list_display = ('user', 'content', 'created_at')
    list_filter = ('user', 'created_at')
    search_fields = ('user', 'content')
    ordering = ('created_at',)


admin.site.register(Tweet, TweetAdmin)