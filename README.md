# Django Twitter

A simple clone of twitter using Django.

## Functionality
- User Signup, Login, Logout, Settings, Change Password
    + Using `django.contrib.auth`
- Tweeting with Mentions
- Following other users

## Known Issues / TODOs
- User Interface needs to be implemented in bootstrap
- Feed needs to be updated to include follower's tweets
- Need to handle deleting user properly
- Convert function-based-views to class-based-views
- Mentioning the same user 2 times breaks the templatetag, needs fix