from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^register/', 'users.views.register', name='register'),
    url(r'^settings/', 'users.views.settings', name='settings'),
    url(r'^login/', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    url(r'^logout/', 'django.contrib.auth.views.logout', {'next_page': 'home'}),
    url(r'^delete/', 'users.views.delete', name='delete'),
    url(r'^people/', 'users.views.people', name='people'),
    url(r'^profile/(?P<username>[\w.@+-]+)/$', 'users.views.profile', name='profile'),
    url(r'^follow/(?P<username>[\w.@+-]+)/$', 'users.views.follow', name='follow'),
    url(r'^unfollow/(?P<username>[\w.@+-]+)/$', 'users.views.unfollow', name='unfollow'),
    url(r'^password/done/', 'django.contrib.auth.views.password_change_done',
        {'template_name': 'change_password_done.html'}),
    url(r'^password/', 'django.contrib.auth.views.password_change',
        {'template_name': 'change_password.html',
         'post_change_redirect': 'users:django.contrib.auth.views.password_change_done'}),
)