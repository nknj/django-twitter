# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Follow.following'
        db.delete_column(u'users_follow', 'following_id')

        # Deleting field 'Follow.follower'
        db.delete_column(u'users_follow', 'follower_id')

        # Adding field 'Follow.followed_by'
        db.add_column(u'users_follow', 'followed_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, related_name='followed_by', to=orm['users.TwitterUser']),
                      keep_default=False)

        # Adding field 'Follow.followed'
        db.add_column(u'users_follow', 'followed',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, related_name='followed', to=orm['users.TwitterUser']),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Follow.following'
        raise RuntimeError("Cannot reverse this migration. 'Follow.following' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Follow.following'
        db.add_column(u'users_follow', 'following',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='followed_by', to=orm['users.TwitterUser']),
                      keep_default=False)

        # Adding field 'Follow.follower'
        db.add_column(u'users_follow', 'follower',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, related_name='followed', to=orm['users.TwitterUser']),
                      keep_default=False)

        # Deleting field 'Follow.followed_by'
        db.delete_column(u'users_follow', 'followed_by_id')

        # Deleting field 'Follow.followed'
        db.delete_column(u'users_follow', 'followed_id')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.follow': {
            'Meta': {'object_name': 'Follow'},
            'followed': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'followed'", 'to': u"orm['users.TwitterUser']"}),
            'followed_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'followed_by'", 'to': u"orm['users.TwitterUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'users.twitteruser': {
            'Meta': {'object_name': 'TwitterUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'following': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['users.TwitterUser']", 'through': u"orm['users.Follow']", 'symmetrical': 'False'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'one_liner': ('django.db.models.fields.CharField', [], {'max_length': '140', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['users']