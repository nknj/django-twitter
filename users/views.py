from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.forms import model_to_dict
from django.shortcuts import redirect, render, get_object_or_404
from users.forms import UserCreationForm, UserSettingsForm
from users.models import TwitterUser, Follow


def register(request):
    if request.method == 'POST':
        register_form = UserCreationForm(request.POST)
        if register_form.is_valid():
            register_form.save()
            new_user = authenticate(
                username=register_form.cleaned_data['email'],
                password=register_form.cleaned_data['password1'])
            login(request, new_user)
            return redirect('home')
        else:
            return render(request, 'register.html', {'register_form': register_form})
    elif request.method == 'GET':
        register_form = UserCreationForm()
        return render(request, 'register.html', {'register_form': register_form})


@login_required
def settings(request):
    if request.method == 'POST':
        settings_form = UserSettingsForm(request.POST, instance=request.user)
        if settings_form.is_valid():
            settings_form.save()
            return redirect('users:settings')
        else:
            return render(request, 'settings.html', {'settings_form': settings_form})
    elif request.method == 'GET':
        settings_form = UserSettingsForm(initial=model_to_dict(request.user))
        return render(request, 'settings.html', {'settings_form': settings_form})


@login_required
def delete(request):
    if request.method == 'POST':
        request.user.is_active = False
        request.user.save()
        logout(request)
        # TODO: archive tweets?
        # TODO: archive follows?
        return redirect('home')
    elif request.method == 'GET':
        return render(request, 'delete.html')


def profile(request, username):
    profile_user = get_object_or_404(TwitterUser, username=username)
    following, followed_by, self_profile = False, False, False
    if request.user.is_authenticated():
        following = request.user.is_following(profile_user)
        followed_by = request.user.is_followed_by(profile_user)
        self_profile = request.user == profile_user
    return render(request, 'profile.html', {'profile_user': profile_user,
                                            'following': following,
                                            'followed_by': followed_by,
                                            'self_profile': self_profile})

@login_required
def follow(request, username):
    user_to_follow = get_object_or_404(TwitterUser, username=username)
    if not request.user.is_following(user_to_follow):
        f = Follow(followed_by=request.user, followed=user_to_follow)
        f.save()
    return redirect('users:profile', username=username)


@login_required
def unfollow(request, username):
    user_to_unfollow = get_object_or_404(TwitterUser, username=username)
    if request.user.is_following(user_to_unfollow):
        f = Follow.objects.get(followed_by=request.user, followed=user_to_unfollow)
        f.delete()
    return redirect('users:profile', username=username)


@login_required
def people(request):
    return render(request, 'people.html')
