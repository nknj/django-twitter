from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render
from tweets.views import feed
from users.forms import UserCreationForm


def home(request):
    if request.user.is_anonymous():
        register_form = UserCreationForm()
        auth_form = AuthenticationForm()
        return render(request, 'home.html',
                      {'register_form': register_form,
                       'auth_form': auth_form})
    else:
        return feed(request)
