from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', 'twitter.views.home', name='home'),

    # Includes to other apps
    url(r'^user/', include('users.urls', namespace='users')),
    url(r'^tweets/', include('tweets.urls', namespace='tweets')),
    url(r'^admin/', include(admin.site.urls)),
)
